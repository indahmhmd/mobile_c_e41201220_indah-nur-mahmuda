import 'dart:io';

void main() {
  var tgl = 3;
  var bln = 9;
  var thn = 1993;

  String output;

  if (tgl >= 1 && tgl <= 31 && thn >= 1900 && thn <= 2200) {
    switch (bln) {
      case 1:
        {
          output = "$tgl Januari $thn";
          break;
        }
      case 2:
        {
          output = "$tgl Februari $thn";
          break;
        }
      case 3:
        {
          output = "$tgl Maret $thn";
          break;
        }
      case 4:
        {
          output = "$tgl April $thn";
          break;
        }
      case 5:
        {
          output = "$tgl Mei $thn";
          break;
        }
      case 6:
        {
          output = "$tgl Juni $thn";
          break;
        }
      case 7:
        {
          output = "$tgl Juli $thn";
          break;
        }
      case 8:
        {
          output = "$tgl Agustus $thn";
          break;
        }
      case 9:
        {
          output = "$tgl September $thn";
          break;
        }
      case 10:
        {
          output = "$tgl Oktober $thn";
          break;
        }
      case 11:
        {
          output = "$tgl November $thn";
          break;
        }
      case 12:
        {
          output = "$tgl Desember $thn";
          break;
        }
      default:
        {
          output = "Tanggal tidak tersedia!";
        }
    }
    print(output);
  } else {
    print("Data tidak tersedia!");
  }
}
