import 'dart:io';

void main() {
  print("----WEREWOLF----");

  stdout.write("masukan username : ");
  String usrn = stdin.readLineSync()!;
  if (usrn == "") {
    print("Nama harus diisi!!");
  }

  stdout.write("Hello $usrn pilih peran kamu : ");
  String peran = stdin.readLineSync()!;
  if (peran == "") {
    print("Peran harus diisi!!");
  }

  if (peran == "" || usrn == "") {
    print("Data tidak boleh kosong");
  } else if (peran == "Penyihir") {
    print(
        "Selamat datang di Werewolf! Hello $peran $usrn, kamu dapat melihat werewolf!");
  } else if (peran == "Penjaga") {
    print(
        "Selamat datang di Werewolf! Hello $peran $usrn, kamu dapat melindungi teman dari serangan werewolf!");
  } else if (peran == "Werewolf") {
    print(
        "Selamat datang di Werewolf! Hello $peran $usrn, kamu dapat memangsa target dimalam hari!");
  } else {
    print("Mohon mengisi peran");
  }
}
