import 'dart:io';

void main() {
  print("Nilai A :");
  int a = int.parse(stdin.readLineSync()!);
  print("Nilai B :");
  int b = int.parse(stdin.readLineSync()!);

  //penjumlahan
  int hasil_jumlah = a + b;
  print("Hasil penjumlahan = $hasil_jumlah");

  //pengurangan
  int hasil_pengurangan = a - b;
  print("Hasil pengurangan = $hasil_pengurangan");

  //perkalian
  int hasil_kali = a * b;
  print("Hasil perkalian = $hasil_kali");

  //pembagian
  double hasil_bagi = a / b;
  print("Hasil pembagian = $hasil_bagi");
}
