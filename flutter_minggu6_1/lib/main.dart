import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                      begin: FractionalOffset.bottomRight,
                      end: FractionalOffset.topLeft,
                      colors: [
                        Colors.lightBlue,
                        Colors.blueAccent,
                        Colors.purpleAccent,
                      ]),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new RadialGradient(radius: 0.9, colors: [
                    Color(0xff737dfe),
                    Color(0xffffcac9),
                  ]),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new SweepGradient(startAngle: 1.0, colors: [
                    Color(0xffb6c0c5),
                    Color(0xff112d60),
                  ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
