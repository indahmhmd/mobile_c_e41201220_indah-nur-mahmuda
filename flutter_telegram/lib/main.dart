import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/Tele/Telegram.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Telegram',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: Telegram(),
    );
  }
}
