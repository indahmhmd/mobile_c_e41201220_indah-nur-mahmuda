import 'package:flutter/material.dart';
import 'listvalues.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var controller = PageController();
  int currentpage = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Text("$currentpage/" + listofvalue.length.toString()),
          Expanded(
            child: PageView.builder(
                controller: controller,
                itemCount: listofvalue.length,
                itemBuilder: (context, index) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 100, vertical: 100),
                      child: Card(
                        elevation: 6.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        color: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      listofvalue[index].imagepath))),
                        ),
                      ));
                }),
          ),
        ],
      ),
    );
  }
}
