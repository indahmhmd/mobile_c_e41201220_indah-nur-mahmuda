void main(List<String> args) async {
  print("Ready. Start");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
  print(await line8());
  print(
      "==Salem Ilese, TOMORROW X TOGETHER - PS5 (Lyrics) feat. Alan Walker==");
}

Future<String> line() async {
  String Lyric = "It's me and the PS5";
  return await Future.delayed(Duration(seconds: 1), () => (Lyric));
}

Future<String> line2() async {
  String Lyric = "Tell me, why you makin' me decide? (Yeah)";
  return await Future.delayed(Duration(seconds: 2), () => (Lyric));
}

Future<String> line3() async {
  String Lyric = "Please, don't you see I'm live? (I'm live)";
  return await Future.delayed(Duration(seconds: 3), () => (Lyric));
}

Future<String> line4() async {
  String Lyric = "So if you don't leave, I can find a way to win you over";
  return await Future.delayed(Duration(seconds: 3), () => (Lyric));
}

Future<String> line5() async {
  String Lyric = "Me and the PS5";
  return await Future.delayed(Duration(seconds: 3), () => (Lyric));
}

Future<String> line6() async {
  String Lyric = "Least you know where I'ma be at night (Yeah)";
  return await Future.delayed(Duration(seconds: 2), () => (Lyric));
}

Future<String> line7() async {
  String Lyric = "The score's never been this high (So high)";
  return await Future.delayed(Duration(seconds: 3), () => (Lyric));
}

Future<String> line8() async {
  String Lyric = "So if you don't leave, I can find a way to win you over";
  return await Future.delayed(Duration(seconds: 3), () => (Lyric));
}
