import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dropdown"),
      ),
      body: Center(
        child: DropdownSearch<String>(
          mode: Mode.MENU,
          showSelectedItems: true,
          items: ["Taco", "Ramen (Disable)", "Pizza", "Sate", "Lasagna"],
          label: "Food List",
          hint: "Food in the list",
          popupItemDisabled: (String s) => s.startsWith('R'),
          onChanged: print,
          selectedItem: "Taco",
        ),
      ),
    );
  }
}
