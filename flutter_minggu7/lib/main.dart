import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(
    title: "Minggu 7",
    home: BelajarMinggu7(),
  ));
}

class BelajarMinggu7 extends StatefulWidget {
  @override
  _BelajarMinggu7State createState() => _BelajarMinggu7State();
}

class _BelajarMinggu7State extends State<BelajarMinggu7> {
  double nilaiSlider = 25;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Belajar Minggu 7"),
      ),
      body: Form(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "Fill this field",
                  labelText: "Full Name",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please fill this field';
                  }
                  return null;
                },
              ),
              CheckboxListTile(
                title: Text('Get update'),
                subtitle: Text('You will get the latest update information'),
                value: true,
                activeColor: Colors.blue,
                onChanged: (value) {},
              ),
              SwitchListTile(
                title: Text('Turn on notification'),
                subtitle: Text('Get notification'),
                value: true,
                activeTrackColor: Colors.blueGrey,
                activeColor: Colors.blue,
                onChanged: (value) {},
              ),
              Slider(
                value: nilaiSlider,
                min: 0,
                max: 100,
                onChanged: (value) {
                  setState(() {
                    nilaiSlider = value;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
