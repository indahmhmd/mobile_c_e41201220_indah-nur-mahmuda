import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(HalloApp());
}

class HalloApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Hallo App",
        home: Scaffold(
          appBar: AppBar(
            title: Text('Login'),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(hintText: "Masukan username"),
                    ),
                    TextFormField(
                      decoration: InputDecoration(hintText: "Masukan Password"),
                    ),
                    ButtonTheme(
                      child: RaisedButton(
                        onPressed: () {},
                        child: Text('Login'),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
